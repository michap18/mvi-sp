import numpy as np

import constants
from model import DecisionModel
from database import SampleDatabase
from observation import Observation
from action import Action
from reward import RewardFunction
from victory_func import VictoryFunc
from sample import Sample


class Agent:
    def __init__(self, external_decision_model: DecisionModel = None, sample_database: SampleDatabase = None,
                 reward_function: RewardFunction = None, trainable_model: bool = False):
        self.decision_model = external_decision_model
        self.sample_database = sample_database
        self.reward_function = reward_function
        if not sample_database:
            self.sample_database = SampleDatabase()
        self.trainable_model = trainable_model

    def choose_action(self, possible_actions: list,  observation: Observation):
        action, _ = self.decision_model.choose_action(possible_actions, observation)
        return action, True

    def train(self, original_observation: Observation, action: Action, current_observation: Observation,
              victory_func: VictoryFunc):
        s1 = original_observation.transform_to_one_hot_vector()
        s2 = current_observation.transform_to_one_hot_vector()
        a = action.transform_to_one_hot_vector()
        r = self.reward_function.calculate_reward(current_observation, victory_func)

        sample = Sample(s1, a, r, s2)
        self.sample_database.add_sample(sample)
        if not self.trainable_model:
            return
        else:
            self.decision_model.steps += 1
            if self.decision_model.use_epsilon:
                self.decision_model.epsilon = constants.MIN_EPSILON + (constants.MAX_EPSILON - constants.MIN_EPSILON) * \
                               np.exp(-constants.LAMBDA*self.decision_model.steps)

            self.replay()
        return

    def replay(self):
        samples = self.sample_database.random_sample(constants.BATCH_SIZE)
        original_states = np.array([x.state_original for x in samples])
        final_states = np.array([x.state_final for x in samples])

        original_predictions = self.decision_model.predict(original_states)
        final_predictions = self.decision_model.predict(final_states)
        training_data = np.zeros((constants.BATCH_SIZE, 1096))
        test_data = np.zeros((constants.BATCH_SIZE, 1152))

        for i in range(constants.BATCH_SIZE):
            sample = samples[i]

            s1 = sample.state_original
            a = sample.action
            a_idx = np.where(a == 1)[0]
            if len(a_idx) != 1:
                print(a_idx)
                raise ValueError
            a_idx = a_idx[0]
            r = sample.reward
            s2 = sample.state_final

            target = original_predictions[i]
            target[a_idx] = r + constants.GAMMA + np.max(final_predictions)

            training_data[i] = s1
            test_data[i] = target

        self.decision_model.train(training_data, test_data)
