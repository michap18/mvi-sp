from line_position import LinePosition


def get_bounded_cards(original_line: list, position: LinePosition, original_card_type: int) -> tuple:
    line = original_line.copy()
    if position == LinePosition.START:
        bounded_cards = []
        take_bounded_cards = False
        last_bounded_card_idx = -1

        i = -1
        for card_type in line:
            i += 1
            if card_type != original_card_type:
                bounded_cards.append(card_type)
            else:
                take_bounded_cards = True
                last_bounded_card_idx = i - 1
                break

        return take_bounded_cards, bounded_cards, last_bounded_card_idx
    else:
        bounded_cards = []
        take_bounded_cards = False
        last_bounded_card_idx = -1

        i = len(line)
        while i > 0:
            i -= 1
            card_type = line[i]
            if card_type != original_card_type:
                bounded_cards.append(card_type)
            else:
                take_bounded_cards = True
                last_bounded_card_idx = i
                break
        return take_bounded_cards, bounded_cards, last_bounded_card_idx


