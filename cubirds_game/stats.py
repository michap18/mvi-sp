import time

from model import DecisionModel, HiddenLayerModel, RandomDecisionModel
from game_state import GameState
from variant import Variant


def measure_stats(model1, model2, sample_size: int, winner_sample_size: int,
                  variant: Variant):
    winner_samples = 0
    samples = 0
    num_of_turns = []
    max_line_size = 0
    victory_types_stats = dict()
    flock_cnt = 0

    game_state = GameState(variant, write_to_log=False)
    game_state.players[0].decision_model = model1
    game_state.players[1].decision_model = model2

    i = -1
    while True:
        i += 1
        if winner_samples >= winner_sample_size and samples >= sample_size:
            break
        game_state.play_game()
        if game_state.max_line_size > max_line_size:
            max_line_size = game_state.max_line_size
        if game_state.winner == game_state.players[0]:
            winner_samples += 1
            num_of_turns.append(game_state.turn)
        samples += 1
        if victory_types_stats.get(game_state.victory_type) is None:
            victory_types_stats[game_state.victory_type] = 1
        else:
            victory_types_stats[game_state.victory_type] += 1
        flock_cnt += sum(game_state.players[0].collection)

    winrate = (winner_samples / samples) * 100
    avg_turn = sum(num_of_turns) / len(num_of_turns)
    v_stat = (victory_types_stats['7 different flocks victory'] / samples) * 100
    avg_flock_cnt = flock_cnt / samples
    return winrate, avg_turn, avg_flock_cnt, v_stat


def measure_stats_of_dir_with_models(dir_path: str, min_epizode: int, max_epizode: int, period: int, log_file: str,
                                     variant):
    log_str = ""
    epizode = min_epizode

    while epizode <= max_epizode:
        print(epizode, end=" ")
        start = time.time()
        file = dir_path + str(epizode) + ".h5"
        model = HiddenLayerModel(load_from_file=file)
        win_rate, avg_turn, flock_cnt, v_type = measure_stats(model, RandomDecisionModel(), 1000, 0, variant)
        log_str += str(epizode) + " " + str(win_rate) + " " + str(avg_turn) + " " + str(flock_cnt) + " " + \
                   str(v_type) + "\n"
        with open(log_file, 'w') as f:
            f.write(log_str)
        f.close()
        end = time.time()
        print(end - start, end='\n')
