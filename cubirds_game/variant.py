from __future__ import annotations

from victory_func import VictoryFunc
from type import Type
import math

import json


class Variant:
    def __init__(self, hand_size: int = 0, line_count: int = 0, line_size: int = 0, player_count: int = 0,
                 collection_size: int = 0, victory_func_name: str = "", name: str = ""):
        self.hand_size = hand_size
        self.line_count = line_count
        self.line_size = line_size
        self.player_count = player_count
        self.collection_size = collection_size
        self.types = []
        self.victory_func = VictoryFunc(victory_func_name)
        self.name = name
        self.action_cnt = -1
        self.states_cnt = -1


    @staticmethod
    def import_variant_from_json(file_path: str) -> Variant:
        f = open(file_path)
        data = json.load(f)
        v = Variant(int(data['handSize']), int(data['lineCount']), int(data['lineSize']), int(data['playerCount']),
                    int(data['collectionSize']), data['victoryFunc']['name'], data['variantName'])
        for card_type in data['types']:
            v.add_type(card_type['multiplicity'], card_type['smallFlockSize'], card_type['bigFlockSize'],
                       card_type['typeName'])
        for bound in data['victoryFunc']['bounds']:
            lower_bound = bound['from']
            upper_bound = bound['to']
            need_for_win = bound['needForWin']
            bound_values = []
            for x in [lower_bound, upper_bound, need_for_win]:
                if x == "inf":
                    bound_values.append(math.inf)
                else:
                    bound_values.append(int(x))
            v.victory_func.add_bound(bound_values[0], bound_values[1], bound_values[2], bound['boundName'])
        v.calculate_states_and_action_count()
        return v

    def calculate_states_and_action_count(self):
        self.action_cnt = len(self.types) * self.line_count * 2 * (len(self.types) + 1) * 2
        self.states_cnt = self.calculate_states_cnt()

    def add_type(self, multiplicity: int, small_flock_size: int, big_flock_size: int, name: str):
        type_id = len(self.types) + 1
        t = Type(type_id, multiplicity, small_flock_size, big_flock_size, name)
        self.types.append(t)

    def calculate_states_cnt(self):
        states_cnt = 0
        card_cnt = 0
        for card_type in self.types:
            card_cnt += card_type.multiplicity
        states_cnt += (self.line_count * card_cnt)  # line representation
        states_cnt += (self.player_count * card_cnt)  # hand of players
        states_cnt += card_cnt  # grave_yard
        states_cnt += (self.player_count - 1) * 15  # num of opponents cards
        states_cnt += card_cnt // 10 # cards in decl
        return states_cnt
