class VictoryFunc:
    def __init__(self, name):
        self.name = name
        self.bounds = []

    def add_bound(self, lower_bound: int, upper_bound: int, cards_required: int, name: str) -> None:
        self.bounds.append([lower_bound, upper_bound, cards_required, name])

    def check_collection_for_victory(self, collection: list) -> tuple:
        for bound in self.bounds:
            lower_bound = bound[0]
            cards_required = bound[2]
            card_score = 0
            for card_cnt in collection:
                if card_cnt >= lower_bound:
                    card_score += 1
            if card_score >= cards_required:
                return True, bound[3]

        return False, None
