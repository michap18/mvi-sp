from training import train_model
from stats import measure_stats_of_dir_with_models
from variant import Variant
from game_state import GameState
from model import *
import time


def train_models():
    start = time.time()
    train_model([1152, 1152, 1152], 4000, 10, "F")
    train_model([1152, "d0.2", 1152, "d0.2", 1152], 1000, 20, "A")
    train_model([1152, 1152, 1152, 1152, 1152], 1000, 20, "C")
    train_model([144, 144, 144, 144, 144, 144, 144, 144], 1000, 20, "D")
    train_model([2304, 1152], 1000, 20, "E")
    train_model([2304, 2304, 1152], 1000, 20, "F")
    end = time.time()
    print('Test', end-start)


if __name__ == '__main__':
    train_models()
    #original_variant = Variant.import_variant_from_json("./Data/Variants/original_2players.json")
    #game_state = GameState(original_variant, write_to_log=True, log_file_path='./Log/log1.txt')
    #game_state.players[0].decision_model = HiddenLayerModel(load_from_file='./models/1220.h5')
    #game_state.players[1].decision_model = RandomDecisionModel()
    #game_state.play_game()
    #original_variant = Variant.import_variant_from_json("./Data/Variants/original_2players.json")
    #measure_stats_of_dir_with_models('./models/F_external/', 0, 920, 20, './Log/F_external_training_log.txt', original_variant)

