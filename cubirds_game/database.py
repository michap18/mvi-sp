import constants
import random


class SampleDatabase:
    def __init__(self):
        self.samples = []
        self.capacity = constants.SAMPLE_DATABASE_CAPACITY

    def has_full_capacity(self) -> bool:
        return len(self.samples) >= constants.SAMPLE_DATABASE_CAPACITY

    def add_sample(self, sample):
        self.samples.append(sample)
        if len(self.samples) > constants.SAMPLE_DATABASE_CAPACITY:
            self.samples.pop(0)

    def random_sample(self, size):
        n = min(size, len(self.samples))
        return random.sample(self.samples, n)
