from agent import Agent
from game_state import GameState
from variant import Variant
from model import RandomDecisionModel, HiddenLayerModel
from reward import CollectionRewardFunctionC
from stats import measure_stats
import time


def train_one_game(agent1: Agent, agent2: Agent, variant: Variant):
    game_state = GameState(variant, write_to_log=False)
    game_state.players[0].decision_model = agent1
    game_state.players[1].decision_model = agent2
    game_state.play_game()


def train_model(dense_layers: list, epizodes: int, save_frequency: int, name: str):
    log_file = "./Log/training_log_" + name + ".txt"
    log_str = ""
    original_variant = Variant.import_variant_from_json("./Data/Variants/original_2players.json")
    rng_agent1 = Agent(external_decision_model=RandomDecisionModel(), reward_function=CollectionRewardFunctionC(),
                       trainable_model=False)
    rng_agent2 = Agent(external_decision_model=RandomDecisionModel(), reward_function=CollectionRewardFunctionC(),
                       trainable_model=False)

    while not rng_agent1.sample_database.has_full_capacity() or not rng_agent2.sample_database.has_full_capacity():
        train_one_game(rng_agent1, rng_agent2, original_variant)
    log_str += "Agents initialization complete\n"

    agent1 = Agent(external_decision_model=HiddenLayerModel(use_epsilon=True,  hidden_layers=dense_layers), trainable_model=True,
                   reward_function=CollectionRewardFunctionC())
    agent2 = Agent(external_decision_model=HiddenLayerModel(use_epsilon=True, hidden_layers=dense_layers), trainable_model=True,
                   reward_function=CollectionRewardFunctionC())
    agent1.sample_database = rng_agent1.sample_database
    agent2.sample_database = rng_agent2.sample_database

    for epizode in range(epizodes):
        print("Status report:", name, epizode, agent1.decision_model.epsilon)
        train_one_game(agent1, agent2, original_variant)
        if epizode % save_frequency == 0:
            agent1.trainable_model = False
            agent2.trainable_model = False
            start = time.time()
            win_rate, _, _, _ = measure_stats(agent1, agent2, 101, 0, original_variant)
            if win_rate > 50:
                best_model = agent1
            else:
                best_model = agent2

            win_rate, avg_turn, flock_cnt, v_type = measure_stats(best_model, RandomDecisionModel(), 1000, 0,
                                                                  original_variant)
            best_model.decision_model.save_model("./models/" + name + '/' + str(epizode) + '.h5')
            log_str += str(epizode) + " " + str(win_rate) + " " + str(avg_turn) + " " + str(flock_cnt) + " " + \
                       str(v_type) + "\n"
            agent1.trainable_model = True
            agent2.trainable_model = True

            with open(log_file, "w") as f:
                f.write(log_str)
    f.close()
