from variant import Variant
from player import Player
from action import Action
from helper_func import get_bounded_cards
from line_position import LinePosition
from copy import deepcopy, copy
from observation import Observation
import random
from log import Log


class GameState:
    def __init__(self, variant: Variant, log_file_path: str = None, write_to_log: bool = True, make_steps = False):
        self.variant = variant
        self.players = []
        self.define_players()
        self.table = [[] for _ in range(self.variant.line_count)]
        self.deck = []
        self.on_turn = None
        self.turn = 0
        self.card_count = self.calculate_card_count()
        self.graveyard = []
        self.game_in_progress = False
        self.no_cards = False
        self.round_reset = False
        self.winner = None
        self.victory_type = None
        self.write_to_log = write_to_log
        self.max_line_size = 0
        if self.write_to_log:
            self.log = Log(log_file_path)
        else:
            self.log = None
        self.make_steps = make_steps

    def is_started(self) -> bool:
        return self.game_in_progress

    def start_game(self):
        self.reset()
        if self.is_started():
            print("Game is already started!")
            raise ValueError

        for i in range(len(self.players)):
            player = self.players[i]
            opponent = self.players[1 - i]
            player.observation = Observation(copy(player), self.table, copy(opponent), self.variant.types)
            player.opponent = opponent
        self.set_order()
        self.create_deck()
        self.deal_cards_to_players()
        self.init_table()

        self.game_in_progress = True
        self.turn = 1
        if self.write_to_log:
            self.log.write_gamestate(self.table, self.players, self.deck, self.graveyard, self.variant.types)

    def calculate_card_count(self) -> int:
        card_count = 0
        for card_type in self.variant.types:
            card_count += card_type.multiplicity
        return card_count

    def set_order(self) -> None:
        priorities = list(range(1, self.variant.player_count + 1))
        order = [0] * self.variant.player_count
        i = 0
        while len(priorities) > 0:
            seed = random.randint(0, len(priorities) - 1)
            p = priorities[seed]
            self.players[i].priority = p
            order[self.variant.player_count - p] = i
            priorities.pop(seed)
            i += 1

        for i in range(len(order)):
            if i == (len(order) - 1):
                self.players[i].next_player = self.players[0]
            else:
                self.players[i].next_player = self.players[i + 1]
        self.on_turn = 0

    def reset(self):
        self.table = [[] for _ in range(self.variant.line_count)]
        self.deck = []
        self.graveyard = []
        self.on_turn = None
        self.turn = 0
        self.no_cards = True
        self.round_reset = False
        self.victory_type = None
        self.winner = None
        self.max_line_size = 0

        for player in self.players:
            player.reset()

    def define_players(self):
        for i in range(self.variant.player_count):
            name = chr(65 + i)
            self.players.append(Player(self.variant, name))

    def create_deck(self):
        cards = []
        for i in range(len(self.variant.types)):
            card_type = self.variant.types[i]
            for j in range(card_type.multiplicity):
                cards.append(i)

        while len(cards) > 0:
            seed = random.randint(0, len(cards) - 1)
            self.deck.append(cards[seed])
            cards.pop(seed)

        self.no_cards = False

    def deal_cards_to_players(self):
        for i in range(self.variant.hand_size):
            for player in self.players:
                self.draw_a_card_from_deck(player)

        for i in range(self.variant.collection_size):
            for player in self.players:
                self.add_card_to_collection_from_deck(player)

    def get_top_card_from_deck(self) -> int:
        if len(self.deck) == 0:  # deck is empty
            enough_cards = self.shuffle_graveyard_into_deck()
            if not enough_cards:
                self.no_cards = True
                return -1

        card_type = self.deck[0]
        self.deck.pop(0)
        return card_type

    def draw_a_card_from_deck(self, player: Player) -> None:
        card_type = self.get_top_card_from_deck()
        if self.write_to_log:
            self.log.write_card_draw(player.name, card_type)
        if self.no_cards:
            return
        else:
            player.hand[card_type] += 1
            player.observation.potential_card_in_deck[card_type] -= 1

    def shuffle_graveyard_into_deck(self) -> bool:
        if len(self.graveyard) == 0:
            return False

        while len(self.graveyard) > 0:
            seed = random.randint(0, len(self.graveyard) - 1)
            self.deck.append(self.graveyard[seed])
            self.graveyard.pop(seed)

        for player in self.players:
            player.observation.graveyard = [0] * len(self.variant.types)
            player.observation.potential_card_in_deck = [0] * len(self.variant.types)
            for card_type in self.deck:
                player.observation.potential_card_in_deck[card_type] += 1
        if self.write_to_log:
            self.log.write_to_log("Shuffling graveyard into deck")
        return True

    def add_card_to_collection_from_deck(self, player: Player):
        card_type = self.get_top_card_from_deck()
        if self.no_cards:
            return
        else:
            player.collection[card_type] += 1

        for player in self.players:
            player.observation.potential_card_in_deck[card_type] -= 1

    def init_table(self):
        for i in range(self.variant.line_count):
            layed_types = []
            while len(layed_types) < self.variant.line_size:
                card_type = self.get_top_card_from_deck()
                for player in self.players:
                    player.observation.potential_card_in_deck[card_type] -= 1

                if card_type in layed_types:
                    self.graveyard.append(card_type)
                    for player in self.players:
                        player.observation.graveyard[card_type] += 1
                else:
                    self.add_card_to_line_from_deck(i, True, card_type)
                    layed_types.append(card_type)

    def add_card_to_line_from_deck(self, line_idx: int, to_end: bool, card_type: int = None) -> None:
        if card_type is None:
            card_type = self.get_top_card_from_deck()
            for player in self.players:
                player.observation.potential_card_in_deck[card_type] -= 1
            if self.no_cards:
                return

        if to_end:
            self.table[line_idx].append(card_type)
        else:
            self.table[line_idx].insert(0, card_type)

    def game_loop(self):
        end_flag = False
        while not end_flag:
            """
            print("----------------- Turn", self.turn, " -----------------------")
            for player in self.players:
                print(player.name, "s Observation:")
                print('Graveyard', player.observation.graveyard)
                print('Deck', player.observation.potential_card_in_deck)
                print('Hand', player.observation.my_hand)
                print('Collection', player.observation.my_collection)
                print('Enemy hand', player.observation.opponents_hand)
                print('Enemy collection', player.observation.opponent_collection)
                print('Table', player.observation.table)
                print("\n")
            """
            for _ in range(self.variant.player_count):
                while True:
                    max_line_size = max(
                        [len(self.table[0]), len(self.table[1]), len(self.table[2]), len(self.table[3])])
                    if max_line_size > self.max_line_size:
                        self.max_line_size = max_line_size
                    if len(self.deck) < 2:
                        self.shuffle_graveyard_into_deck()
                    if len(self.deck) < 2:
                        self.no_cards = True
                        self.end_flag = True
                        self.game_in_progress = False
                        break
                    action, trainable = self.players[self.on_turn].choose_action(self.table, self.deck, self.variant.types)
                    original_observation = deepcopy(self.players[self.on_turn].observation)
                    self.evaluate_action(action, self.players[self.on_turn])
                    self.fix_line(action.line_num)
                    if trainable:
                        self.players[self.on_turn].train(original_observation, action, self.variant.victory_func)

                    if self.players[self.on_turn].has_empty_hand():
                        self.round_reset = True
                    if self.no_cards or self.check_victory():
                        end_flag = True
                        break
                    if self.round_reset:
                        self.reset_round()
                        if self.write_to_log:
                            self.log.write_gamestate(self.table, self.players, self.deck, self.graveyard,
                                                     self.variant.types)
                    else:
                        self.on_turn = 1 - self.on_turn
                        break
                if end_flag:
                    break
                if self.write_to_log:
                    self.log.write_gamestate(self.table, self.players, self.deck, self.graveyard, self.variant.types)
            self.turn += 1
            if self.write_to_log:
                self.log.write_to_log("\nTurn n." + str(self.turn))
            if not self.game_in_progress:
                break

    def check_victory(self) -> bool:
        for player in self.players:
            player_winned, victory_type = self.variant.victory_func.check_collection_for_victory(player.collection)
            if player_winned:
                self.victory_type = victory_type
                self.winner = player
                return True
        return False

    def end_game(self):
        if self.no_cards:
            self.define_winner_no_cards()
        if self.write_to_log:
            self.log.write_victory(self.winner)
        self.game_in_progress = False
        return

    def play_game(self):
        self.start_game()
        self.game_loop()
        self.end_game()

    def evaluate_action(self, action: Action, player: Player) -> None:
        take_bounded_cards, bounded_cards, last_bounded_card_idx = \
            get_bounded_cards(self.table[action.line_num], action.position, action.card_type)
        if take_bounded_cards:
            if self.write_to_log:
                self.log.write_addition_to_line(player.name, bounded_cards, action.card_type,
                                                player.hand[action.card_type],
                                                action.line_num, (action.position == LinePosition.END), action.take_cards)
        else:
            if self.write_to_log:
                self.log.write_addition_to_line(player.name, [], action.card_type, player.hand[action.card_type],
                                                action.line_num, (action.position == LinePosition.END), action.take_cards)

        if take_bounded_cards and len(bounded_cards) > 0:
            card_in_hands = player.hand[action.card_type]
            for card_type in bounded_cards:
                player.hand[card_type] += 1
                player.opponent.observation.opponents_hand[card_type] += 1

            if action.position == LinePosition.START:
                self.table[action.line_num] = self.table[action.line_num][last_bounded_card_idx + 1:]
                for i in range(player.hand[action.card_type]):
                    self.table[action.line_num].insert(0, action.card_type)
                    if player.opponent.observation.opponents_hand[action.card_type] > 0:
                        player.opponent.observation.opponents_hand[action.card_type] -= 1
                    else:
                        player.opponent.observation.potential_card_in_deck[action.card_type] -= 1
            else:
                self.table[action.line_num] = self.table[action.line_num][0:last_bounded_card_idx + 1]
                for _ in range(player.hand[action.card_type]):
                    self.table[action.line_num].append(action.card_type)
                    if player.opponent.observation.opponents_hand[action.card_type] > 0:
                        player.opponent.observation.opponents_hand[action.card_type] -= 1
                    else:
                        player.opponent.observation.potential_card_in_deck[action.card_type] -= 1
            player.hand[action.card_type] -= card_in_hands
            player.opponent.observation.opponents_hand[action.card_type] = max(0,
                                                                               player.opponent.observation.opponents_hand[
                                                                                   action.card_type] - card_in_hands)

        if not take_bounded_cards or not bounded_cards:
            for _ in range(player.hand[action.card_type]):
                self.add_card_to_line_from_deck(action.line_num, action.position == LinePosition.END, action.card_type)
                if player.opponent.observation.opponents_hand[action.card_type] > 0:
                    player.opponent.observation.opponents_hand[action.card_type] -= 1
                else:
                    player.opponent.observation.potential_card_in_deck[action.card_type] -= 1
            player.hand[action.card_type] = 0
            if action.take_cards:
                for i in range(2):
                    self.draw_a_card_from_deck(player)
        if self.no_cards:
            return
        if player.has_empty_hand():
            self.round_reset = True
            return
        if action.flock_type != -1:
            if self.write_to_log:
                self.log.write_collection_addition(player.name,
                                                   player.hand[action.flock_type] >=
                                                   self.variant.types[action.flock_type].big_flock_size,
                                                   player.hand[action.flock_type], action.flock_type)
            card_before = player.hand[action.flock_type]
            card_added = player.add_flock_to_collection(action.flock_type, self.variant.types[action.flock_type])
            for i in range(max(0, card_before - card_added)):
                self.graveyard.append(action.flock_type)
                for player in self.players:
                    player.observation.graveyard[action.flock_type] += 1

        if player.has_empty_hand():
            self.round_reset = True
        return

    def fix_line(self, line_num: int) -> None:
        line_fixed = False
        while not line_fixed:
            if len(self.table[line_num]) > 0:
                one_type = True
                for card_type in self.table[line_num]:
                    if card_type != self.table[line_num][0]:
                        one_type = False
                if not one_type:
                    return
                else:
                    self.add_card_to_line_from_deck(line_num, True, None)
            else:
                self.add_card_to_line_from_deck(line_num, True, None)
            if self.no_cards:
                return

    def reset_round(self):
        self.round_reset = False
        for player in self.players:
            card_type = -1
            for card_cnt in player.hand:
                card_type += 1
                for _ in range(card_cnt):
                    self.graveyard.append(card_type)

                    player.observation.graveyard[card_type] += 1
                    player.opponent.observation.graveyard[card_type] += 1
                    if player.opponent.observation.opponents_hand[card_type] > 0:
                        player.opponent.observation.opponents_hand[card_type] -= 1
                    else:
                        player.opponent.observation.potential_card_in_deck[card_type] -= 1
                player.hand[card_type] = 0
        for player in self.players:
            player.observation.opponents_hand = [0] * len(self.variant.types)

        self.deal_cards_to_players()
        if self.write_to_log:
            self.log.write_reset_round(self.players[self.on_turn].name)

    def define_winner_no_cards(self):
        col1_size = sum(self.players[0].collection)
        col2_size = sum(self.players[1].collection)

        if col1_size > col2_size:
            self.winner = self.players[0]
        elif col2_size > col1_size:
            self.winner = self.players[1]
        else:
            seed = random.randint(0,1)
            self.winner = self.players[seed]
