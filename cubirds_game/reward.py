from victory_func import VictoryFunc
from observation import Observation
import math


class RewardFunction:
    def calculate_reward(self, observation: Observation, victory_func: VictoryFunc):
        pass


class CollectionRewardFunctionA(RewardFunction):
    def calculate_reward(self, observation: Observation, victory_func: VictoryFunc):
        reward = self.calculate_reward_from_collection(observation.my_collection, victory_func)
        return reward

    def calculate_reward_from_collection(self, collection: list, victory_func: VictoryFunc) -> float:
        collection = collection.copy()
        reward = 0

        win, _ = victory_func.check_collection_for_victory(collection)
        if win:
            reward = 300
            return reward / 300

        for i in range(len(collection)):  # If there are more than 3 cards of type in collection, thats useless
            collection[i] = min(collection[i], 3)

        reward += sum(collection)

        zero_cnt = collection.count(0)
        one_cnt = collection.count(1)
        two_cnt = collection.count(2)
        three_cnt = collection.count(3)

        missing_cards1 = zero_cnt - 1
        reward += math.pow((7 - missing_cards1), 2)

        i = 3
        steps_remaining = 2
        missing_cards2 = 6
        for cnt in [three_cnt, two_cnt, one_cnt, zero_cnt]:
            while cnt > 0:
                missing_cards2 -= i
                steps_remaining -= 1
                if steps_remaining == 0:
                    break
            i -= 1
            if steps_remaining == 0:
                break

        reward += math.pow(6 - missing_cards2, 2)
        return reward / 300


class CollectionRewardFunctionB(RewardFunction):
    def calculate_reward(self, observation: Observation, victory_func: VictoryFunc):
        reward1 = self.calculate_reward_from_collection(observation.my_collection, victory_func)
        reward2 = self.calculate_reward_from_hand(observation.my_hand)
        return (0.9 * reward1) + (0.1 * reward2)

    def calculate_reward_from_collection(self, collection: list, victory_func: VictoryFunc) -> float:
        collection = collection.copy()
        reward = 0

        win, _ = victory_func.check_collection_for_victory(collection)
        if win:
            reward = 300
            return reward / 300

        for i in range(len(collection)):  # If there are more than 3 cards of type in collection, thats useless
            collection[i] = min(collection[i], 3)

        reward += sum(collection)

        zero_cnt = collection.count(0)
        one_cnt = collection.count(1)
        two_cnt = collection.count(2)
        three_cnt = collection.count(3)

        missing_cards1 = zero_cnt - 1
        reward += math.pow((7 - missing_cards1), 1.5)

        i = 3
        steps_remaining = 2
        missing_cards2 = 6
        for cnt in [three_cnt, two_cnt, one_cnt, zero_cnt]:
            while cnt > 0:
                missing_cards2 -= i
                steps_remaining -= 1
                if steps_remaining == 0:
                    break
            i -= 1
            if steps_remaining == 0:
                break

        reward += math.pow(6 - missing_cards2, 1.5)
        return reward / 300

    def calculate_reward_from_hand(self, hand: list):
        type_count = 0
        for card_type in hand:
            if card_type > 0:
                type_count += 1
        reward = pow(2, 8 - type_count)

        return reward / 256


class CollectionRewardFunctionC(RewardFunction):
    def calculate_reward(self, observation: Observation, victory_func: VictoryFunc):
        reward = self.calculate_reward_from_collection(observation.my_collection, victory_func)
        return reward

    def calculate_reward_from_collection(self, collection: list, victory_func: VictoryFunc) -> float:
        return sum(collection)
