from helper_func import get_bounded_cards
from line_position import LinePosition
import numpy as np


class Action:
    def __init__(self, line_num: int, position: LinePosition, card_type: int, take_cards: bool, flock_type: int):
        self.line_num = line_num
        self.position = position
        self.card_type = card_type
        self.take_cards = take_cards
        self.flock_type = flock_type

    def __eq__(self, other):
        if self.line_num != other.line_num:
            return False
        if self.take_cards != other.take_cards:
            return False
        if self.position != other.position:
            return False
        if self.card_type != other.card_type:
            return False
        if self.flock_type != other.flock_type:
            return False
        return True

    def __str__(self):
        action_str = ""
        action_str += "Action:\n" \
                      "line num = " + str(self.line_num) + "\n" \
                      "position = " + str(self.position) + "\n" \
                      "card type = " + str(self.card_type) + "\n" \
                      "take cards = " + str(self.take_cards) + "\n" \
                      "flock type = " + str(self.flock_type) + "\n"
        return action_str

    @staticmethod
    def get_all_possible_actions(player_hand: list, desk: list, deck: list, type_list: list) -> list:
        possible_actions = []
        card_type = 0

        for card_type_cnt in player_hand:
            if card_type_cnt > 0:
                for line_num in range(len(desk)):
                    for position in [LinePosition.START, LinePosition.END]:
                        take_bounded_cards, bounded_cards, last_bounded_card_idx = \
                            get_bounded_cards(desk[line_num], position, card_type)

                        if not take_bounded_cards or not bounded_cards:
                            try:
                                action_list = Action.generate_possible_actions(player_hand, [deck[0],  deck[1]], card_type,
                                                                               type_list, line_num, position, True)
                            except IndexError:
                                print(deck)
                                raise IndexError
                            possible_actions.extend(action_list)

                        if take_bounded_cards:
                            action_list = Action.generate_possible_actions(player_hand, bounded_cards, card_type,
                                                                           type_list, line_num, position, False)
                        else:
                            action_list = Action.generate_possible_actions(player_hand, [], card_type,
                                                                           type_list, line_num, position, False)
                        possible_actions.extend(action_list)
            card_type += 1
        return possible_actions

    @staticmethod
    def generate_possible_actions(original_hand: list, additions: list, played_card_type: int, types: list,
                                  line_num: int, position: LinePosition, take_cards: bool) -> list:
        action_list = []
        hand = original_hand.copy()
        hand[played_card_type] = 0
        for card in additions:
            hand[card] += 1

        for card_type_idx in range(len(hand)):
            card_count = hand[card_type_idx]
            card_type = types[card_type_idx]
            if card_count >= card_type.small_flock_size:
                action_list.append(Action(line_num, position, played_card_type, take_cards, card_type_idx))
        action_list.append(Action(line_num, position, played_card_type, take_cards, -1))
        return action_list

    def transform_to_one_hot_vector(self) -> np.ndarray:
        vector = np.zeros(1152)
        n = (self.line_num * 288)
        if self.position == LinePosition.END:
            n += 144
        n += self.card_type * 18
        if self.take_cards:
            n += 9
        n += (self.flock_type + 1)
        vector[n] = 1
        return vector

    @staticmethod
    def create_action_from_one_hot_vector(vector: np.ndarray):
        idxs = np.where(vector == 1)[0]
        if len(idxs) != 1:
            print(idxs)
            print(vector)
            raise ValueError
        n = idxs[0]

        line_num = n // 288
        n = n % 288

        if (n // 144) == 0:
            position = LinePosition.START
        elif (n // 144) == 1:
            position = LinePosition.END
        else:
            print(n)
            raise ValueError
        n = n % 144

        card_type = n // 18
        n = n % 18

        if (n // 18) == 0:
            take_cards = False
        elif (n // 18) == 1:
            take_cards = True
        else:
            print(n)
            raise ValueError
        n = n % 9

        flock_type = n - 1

        return Action(line_num, position, card_type, take_cards, flock_type)
