from enum import Enum


class LinePosition(Enum):
    START = 0
    END = 1
