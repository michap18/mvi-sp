from datetime import datetime
from player import Player


class Log:
    def __init__(self, file_path: str):
        self.file_path = file_path
        self.init_log()

    def write_to_log(self, text: str):
        with open(self.file_path, 'a') as f:
            f.write(text)
        f.close()

    def init_log(self):
        text = "\n" \
               "-------------------------------------------------------------------------------------\n" \
               "---------------                     LOG  START                     ------------------\n" \
               "-------------------------------------------------------------------------------------\n" \
               "\n" \
               "" + str(datetime.now()) + "\n"
        self.write_to_log(text)

    def write_gamestate(self, table: list, players: list, deck: list, graveyard: list, types: list):
        self.write_to_log('\n')
        self.write_decks(deck, graveyard)
        self.write_table(table)
        self.write_types(types)
        for player in players:
            self.write_hand_or_collection(player.name, player.hand, False)
            self.write_hand_or_collection(player.name, player.collection, True)

    def write_table(self, table: list):
        text = "Table:\n"
        for line in table:
            for card_type in line:
                text += str(card_type) + "  "
            text += "\n"
        text += '\n'
        self.write_to_log(text)

    def write_hand_or_collection(self, player_name: str, cards: list, collection: bool):
        if collection:
            text = "Collection of player " + player_name + ":  "
        else:
            text = "Hand of player " + player_name + ":  "

        for i in range(len(cards)):
            card_cnt = cards[i]
            text += '[' + str(i) + ']: ' + str(card_cnt) + ';  '
        text += '\n'
        self.write_to_log(text)

    def write_card_draw(self, player_name: str, drawed_card: int):
        text = "Player " + player_name + " drawed card " + str(drawed_card) + " from deck\n"
        self.write_to_log(text)

    def write_collection_addition(self, player_name: str, big_flock: bool, num_of_cards: int, card_type: int):
        text = "Player " + player_name + " lays out " + str(num_of_cards) + " of type " + str(card_type) + \
               " and so he add "
        if big_flock:
            text += 'big'
        else:
            text += 'small'
        text += ' flock to his collection\n'
        self.write_to_log(text)

    def write_addition_to_line(self, player_name: str, bounded_cards: list, played_card: int, num_of_played_card: int,
                               line_num: int, on_end: bool, take_cards: bool):
        text = "\nPlayer " + player_name + " played " + str(num_of_played_card) + " of type " + str(played_card) + \
                " at the "
        if on_end:
            text += 'end'
        else:
            text += 'start'
        text += " of " + str(line_num + 1) + ". line\n"
        text += "By this action he bounds "
        if len(bounded_cards) > 0:
            text += str(len(bounded_cards)) + " cards ("
            for i in range(len(bounded_cards)):
                if i != 0:
                    text += ", "
                text += str(bounded_cards[i])
            text += ") and he added them to his hand\n"
        else:
            text += 'no cards and he decides '
            if take_cards:
                text += "take 2 cards from top of the deck\n"
            else:
                text += "not to take 2 cards from top of the deck\n"
        self.write_to_log(text)

    def write_reset_round(self, player_name: str):
        text = "Game was reseted cause player " + player_name + " had no cards."
        self.write_to_log(text)

    def write_decks(self, deck: list, graveyard: list):
        text = "There are " + str(len(deck)) + " cards in deck and " + str(len(graveyard)) + " cards in graveyard\n"
        self.write_to_log(text)

    def write_types(self, types: list):
        text = ''
        for i in range(len(types)):
            if i > 0:
                text += " ; "
            text += str(i) + ": [" + str(types[i].small_flock_size) + ',' + str(types[i].big_flock_size) + ']'
        self.write_to_log(text + '\n')

    def write_victory(self, winner: Player):
        text = "\nPlayer " + winner.name + " wins!\n"
        self.write_to_log(text)
