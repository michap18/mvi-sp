import random

import numpy as np

import constants
from action import Action
from random import randint
from observation import Observation

from keras.models import Sequential, load_model
from keras.layers import *
from keras.optimizers import *



class DecisionModel:
    def choose_action(self, possible_actions: list,  observation: Observation) -> tuple:
        pass


class RandomDecisionModel(DecisionModel):
    def choose_action(self, possible_actions: list,  observation: Observation) -> tuple:
        seed = randint(0, len(possible_actions) - 1)
        return possible_actions[seed], False


class HumanDecisionModel(DecisionModel):
    def choose_action(self, possible_actions: list,  observation: Observation) -> tuple:
        pass


class HiddenLayerModel(DecisionModel):
    def __init__(self, hidden_layers: list = [], use_epsilon: bool = False, load_from_file: str = None):
        if not load_from_file:
            self.model = Sequential()
            self.model.add(Dense(units=548, activation='relu', input_dim=1096))
            for layer in hidden_layers:
                if str(layer)[0] == 'd':
                    rate = float(str(layer[1:]))
                    self.model.add(Dropout(rate))
                else:
                    self.model.add(Dense(layer))
            self.model.add(Dense(units=1152, activation='linear'))
            opt = RMSprop(learning_rate=0.0000001)
            self.model.compile(loss='mse', optimizer=opt)
            self.steps = 0
            self.use_epsilon = use_epsilon
            if use_epsilon:
                self.epsilon = 1
            else:
                self.epsilon = None

        else:
            self.model = load_model(load_from_file)
            self.use_epsilon = False

    def train(self, training_data, test_data):
        self.model.fit(training_data, test_data, verbose=2)

    def predict(self, s):
        return self.model.predict(s, verbose=2)

    def predict_one(self, s):
        return self.predict(s.reshape(1, 1096)).flatten()

    def choose_action(self, possible_actions: list, observation: Observation) -> tuple:
        if self.use_epsilon:
            if random.random() < self.epsilon:
                seed = randint(0, len(possible_actions) - 1)
                return possible_actions[seed], False

        s = observation.transform_to_one_hot_vector()
        rating_vector = self.predict_one(s)

        if constants.PRINT_ACTION_SCORES:
            for _ in range(1152):
                idx = np.argmax(rating_vector)
                a = np.zeros(1152)
                a[idx] = 1
                action = Action.create_action_from_one_hot_vector(a)
                if action in possible_actions:
                    print(action)
                    print('Score: ' + str(rating_vector[idx]))
                    print("-----------------------------------------------------------")

                rating_vector[idx] = -np.inf


        while True:
            idx = np.argmax(rating_vector)
            a = np.zeros(1152)
            a[idx] = 1
            action = Action.create_action_from_one_hot_vector(a)
            if action in possible_actions:
                return action, False
            else:
                rating_vector[idx] = -np.inf

    def save_model(self, file_path):
        self.model.save(file_path)


