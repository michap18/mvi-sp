class Type:
    def __init__(self, type_id: int, multiplicity: int, small_flock_size: int, big_flock_size: int, name: str = ""):
        self.id = type_id
        self.multiplicity = multiplicity
        self.small_flock_size = small_flock_size
        self.big_flock_size = big_flock_size
        self.name = name
