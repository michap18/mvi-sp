from copy import copy
from player import Player
import numpy as np


class Observation:
    def __init__(self, player: Player, table: list, opponent: Player, types: list):
        self.my_hand = player.hand
        self.my_collection = player.collection
        self.opponent_collection = opponent.collection

        self.potential_card_in_deck = []
        for card_type in types:
            self.potential_card_in_deck.append(card_type.multiplicity)

        self.graveyard = [0] * len(types)
        self.opponents_hand = [0] * len(types)
        self.table = table
        self.multiplicities = []
        for card_type in types:
            self.multiplicities.append(card_type.multiplicity)

    def transform_to_one_hot_vector(self) -> np.ndarray:
        v = np.zeros(1096)
        for i in range(4):
            line = self.table[i]
            line_vector = self.transform_line_to_one_hot_vector(line)
            v[(i * 136): ((i + 1) * 136)] = line_vector
        n = 4 * 136

        hand_vector = self.transform_card_list_to_one_hot_vector(self.my_hand)
        v[n:n+118] = hand_vector
        n += 118
        opp_hand_vector = self.transform_card_list_to_one_hot_vector(self.opponents_hand)
        v[n:n+118] = opp_hand_vector
        n += 118

        my_coll_vec = self.transform_collection_to_one_hot_vector(self.my_collection)
        op_coll_vec = self.transform_collection_to_one_hot_vector(self.opponent_collection)
        v[n:n+40] = my_coll_vec
        v[n+40:n+80] = op_coll_vec
        n = n + 80

        deck_vec = self.transform_card_list_to_one_hot_vector(self.potential_card_in_deck)
        grave_vec = self.transform_card_list_to_one_hot_vector(self.graveyard)
        v[n:n+118] = deck_vec
        n += 118
        v[n:n+118] = grave_vec

        return v

    def transform_line_to_one_hot_vector(self, line: list) -> np.ndarray:
        v = np.zeros(136)

        types = []
        for i in range(8):
            types.append([-1, -1])  # [position, card count]

        pos = 0
        for card_type in line:
            if types[card_type][0] == -1:
                types[card_type] = [pos, 0]
                pos += 1
            types[card_type][1] += 1

        i = -1
        for pair in types:
            i += 1
            pos = pair[0]
            cnt = min(pair[1], 9) - 1

            if pos != -1:
                v[(i * 17) + pos] = 1
                v[(i * 17) + 8 + cnt] = 1
        return v

    def transform_card_list_to_one_hot_vector(self, card_list: list) -> np.ndarray:
        v = np.zeros(118)

        i = -1
        m_sum = 0
        for m in self.multiplicities:
            i += 1
            v[m_sum + card_list[i]] = 1
            m_sum += m + 1
        return v

    def transform_collection_to_one_hot_vector(self, collection: list) -> np.ndarray:
        v = np.zeros(40)

        n = 0
        for _ in collection:
            card_cnt = min(collection[n//5], 4)
            v[card_cnt] = 1
            n += 5
        return v
