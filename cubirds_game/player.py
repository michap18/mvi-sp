from type import Type
from action import Action


class Player:
    def __init__(self, variant, name=""):
        self.priority = 0
        self.variant = variant
        self.hand = [0] * len(self.variant.types)
        self.collection = [0] * len(self.variant.types)
        self.next_player = None
        self.decision_model = None
        self.name = name
        self.observation = None
        self.opponent = None

    def reset(self):
        self.hand = [0] * len(self.variant.types)
        self.collection = [0] * len(self.variant.types)
        self.priority = 0
        self.next_player = None

    def add_card_to_hand(self, card_type: Type):
        self.hand[card_type.id - 1] += 1

    def add_card_to_collection(self, card_type: Type):
        self.collection[card_type.id - 1] += 1

    def choose_action(self, table: list, deck: list, types: list) -> tuple:
        possible_action_list = Action.get_all_possible_actions(self.hand, table, deck, types)
        if len(possible_action_list) <= 0:
            raise ValueError
        action, trainable = self.decision_model.choose_action(possible_action_list, self.observation)
        return action, trainable

    def add_flock_to_collection(self, type_idx: int, card_type: Type) -> int:
        card_count = self.hand[type_idx]
        for _ in range(card_count):
            if self.opponent.observation.opponents_hand[type_idx] > 0:
                self.opponent.observation.opponents_hand[type_idx] -= 1
            else:
                self.opponent.observation.potential_card_in_deck[type_idx] -= 1
        if card_count >= card_type.big_flock_size:
            self.collection[type_idx] += 2
            card_added = 2
            self.hand[type_idx] = 0
        elif card_count >= card_type.small_flock_size:
            self.collection[type_idx] += 1
            card_added = 1
            self.hand[type_idx] = 0
        else:
            raise ValueError("Cannot add to collection, not enought cards of that type")
        return card_added

    def has_empty_hand(self):
        for card_cnt in self.hand:
            if card_cnt != 0:
                return False
        return True

    def train(self, original_observation, action, victory_func):
        self.decision_model.train(original_observation, action, self.observation, victory_func)
