Zad�n� pr�ce:
Moje zad�n� je vlastn� a je to nau�it model hr�t co nejl�pe karetn� hru Krab��ci. Samotn�
�kol d�l�m v r�mci moj� diplomov� pr�ce https://projects.fit.cvut.cz/theses/4446, kde jsem se zat�m zab�val pouze teori� slo�itosti hry Krab��ci a toto bude za��tek znalostn�-in�en�rsk� ��sti pr�ce. Budu vyu��vat modern� metodu reinforcement learning. Po��te�n� model bude d�lat n�hodn� rozhodnut� a pot� se bude u�it v hran� s�m se sebou. K implementaci bude pou�it jazyk python a pro vytvo�en� neuronov�ch s�t� vyu�iji knihovnu TensorFlow a Keras. C�l je vytvo�it model, kter� dok�e hr�t hru stejn� kvalitn� jako �lov�k a ide�ln� ho i p�ed��. Pravidla hry naleznete zde: https://www.zatrolene-hry.cz/spolecenska-hra/krabcaci-8420/k-stazeni/ .


Data:
��dn� extern� data jsem v�pr�ci nepou��val, model se u�il hran�m s�m se sebou.

Pokyny k spu�t�n�:
Tr�nov�n� se neprov�d� p�es notebook, ale p�es pythonovsk� skript. Ten najdete ve slo�ce ./cubirds_game/main.py, tedy cel� p��kaz pro spu�t�n� je:

python ./cubirds_game/main.py

Je pot�eba m�t nainstalovan� knihovny keras, tensorflow a numpy.